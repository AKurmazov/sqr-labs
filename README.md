[![pipeline status](https://gitlab.com/AKurmazov/sqr-labs/badges/a.kurmazov/lab1/pipeline.svg)](https://gitlab.com/AKurmazov/sqr-labs/-/commits/a.kurmazov/lab1)

I deployed the app via Fly, here is the proof of work:
```
$ curl https://lab1.fly.dev/hello
Hello World!
```
